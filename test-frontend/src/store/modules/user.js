import usuariosAPI from '../../api/usuarios';

export const SET_USERS = 'SET_BOOKS';
export const SET_USER = 'SET_USER';
export const SET_EDIT_USER = 'SET_EDIT_USER';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'

const state = {
    users: [],
    user: null,
    edit: false,
    totalPages: 0
}

const getters = {
    getUsers: (state) => state.usuarios,
    getUser: (state) => state.user,
    getEditUser: (state) => state.edit,
    totalPages: (state) => state.totalPages
}

const mutations = {
    [SET_USERS] (state, users) { 
        state.users = users;
    },
    [SET_USER] (state, user) {
        state.user = user;
    },
    [SET_EDIT_USER](state, edit) {
        state.edit = edit;
    },
    [SET_TOTAL_PAGES] (state, totalPages) {
        state.totalPages = totalPages
    }
}

const actions = {
    actionGetUsers ({ commit }, page) {
        return usuariosAPI.getUsuarios(page)
            .then(response => {
                commit(SET_USERS, response.data.users)
                commit(SET_TOTAL_PAGES, Math.ceil((response.data.totalItems / 6)));
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionGetUser ({ commit }, userId ) {
        return usuariosAPI.getUsuario(userId)
            .then(response => {
                commit(SET_USER, response.data.user)
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionCreateUser ( {commit}, user ) {
        return usuariosAPI.addUsuario(user)
            .then(response => {
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionDeleteUser ({ commit }, userId) {
        return usuariosAPI.deleteUsuario(userId)
            .then(response => {
                return response;
            })
            .catch(err => {
                throw err;
            })
    },
    actionUpdateUser ({ commit }, { userId, usuario }) {
        return usuariosAPI.updateUsuario(userId, usuario)
            .then(response => {
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionSetEditUser({ commit }, edit) {
        commit(SET_EDIT_USER, edit);
    }
}

export default {
    state, 
    getters, 
    actions, 
    mutations
}