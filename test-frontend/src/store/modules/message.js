import Vue from 'vue';

export const SET_MESSAGE = 'SET_MESSAGE';
export const SET_SUCESSO = 'SET_SUCESSO';

const state = {
    message: null,
    sucesso: false
}

const getters = {
    getMessage: (state) => state.message,
    getSucesso: (state) => state.sucesso
}

const mutations = {
    [SET_MESSAGE] (state, message) { 
        state.message = message;
    },
    [SET_SUCESSO] (state, sucesso) { 
        state.sucesso = sucesso;
    }
}

const actions = {
    actionSetMessage ({ commit }, message) {
        commit (SET_MESSAGE, message);
    },
    actionResetMessage ({ commit }) {
        commit (SET_MESSAGE, null);
    },
    actionSetSucesso ({ commit }, sucesso) {
        commit (SET_SUCESSO, sucesso);
    },
    actionResetSucesso ({ commit }) {
        commit (SET_SUCESSO, null);
    },
}

export default {
    state, 
    getters, 
    actions, 
    mutations
}