import Vue from 'vue';

import faturaAPI from '../../api/fatura';

export const SET_FATURAS = 'SET_FATURAS';
export const SET_FATURA = 'SET_FATURA';
export const SET_EDIT_FATURA = 'SET_EDIT_FATURA';
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'

const state = {
    faturas: [],
    fatura: null,
    edit: false,
    totalPages: 0
}

const getters = {
    getFaturas: (state) => state.faturas,
    getFatura: (state) => state.fatura,
    getEdit: (state) => state.edit,
    getTotalPages: (state) => state.totalPages
}

const mutations = {
    [SET_FATURAS](state, faturas) {
        state.faturas = faturas;
    },
    [SET_FATURA](state, fatura) {
        state.fatura = fatura;
    },
    [SET_EDIT_FATURA](state, edit) {
        state.edit = edit;
    },
    [SET_TOTAL_PAGES] (state, totalPages) {
        state.totalPages = totalPages
    }
}

const actions = {
    actionGetFaturas({ commit }, page) {
        return faturaAPI.getFaturas(page)
            .then(response => {
                commit(SET_FATURAS, response.data.faturas);
                commit(SET_TOTAL_PAGES, Math.ceil((response.data.totalItems / 6)));
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionGetFatura({ commit }, faturaId) {
        return faturaAPI.getFatura(faturaId)
            .then(response => {
                commit(SET_FATURA, response.data.fatura)
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionCreateFatura({ commit }, fatura) {
        return faturaAPI.createFatura(fatura)
            .then(response => {
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionDeleteFatura({ commit }, faturaId) {
        return faturaAPI.deleteFatura(faturaId)
            .then(response => {
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionUpdateFatura({ commit }, { faturaId, fatura }) {
        return faturaAPI.updateFatura(faturaId, fatura)
            .then(response => {
                return response;
            })
            .catch(err => {
                return err;
            })
    },
    actionSetEditFatura({ commit }, edit) {
        commit(SET_EDIT_FATURA, edit);
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}