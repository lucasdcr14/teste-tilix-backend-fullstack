import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import fatura from './modules/fatura';
import message from './modules/message';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        user,
        fatura,
        message
    }
});