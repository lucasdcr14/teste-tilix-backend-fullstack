import Home from './components/Home.vue';

import Fatura from './components/Fatura/Fatura.vue';
import Faturas from './components/Fatura/Faturas.vue';
import EditFatura from './components/Fatura/EditFatura.vue';

import Usuario from './components/Usuario/Usuario.vue';
import Usuarios from './components/Usuario/Usuarios.vue';
import EditUsuario from './components/Usuario/EditUsuario.vue';

export const routes = [
    { path: '/', component: Home },
    { path: '/faturas', component: Fatura, children: [
        { path: '', component: Faturas},
        { path: 'edit', component: EditFatura},   
        { path: 'edit/:id', component: EditFatura},
    ] },
    { path: '/usuarios', component: Usuario, children: [
        { path: '', component: Usuarios},
        { path: 'edit', component: EditUsuario},
        { path: 'edit/:id', component: EditUsuario}   
    ] },
    { path: '*', redirect: '/'}
];