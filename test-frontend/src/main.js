import Vue from 'vue'
import VueRouter from 'vue-router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faInfo, faCheck, faTimes, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import money from 'v-money';
import App from './App.vue'
import { routes } from './routes';
import { Vuelidate } from 'vuelidate';
import { store } from './store/store';
import Paginate from 'vuejs-paginate'

library.add(faTrash, faInfo, faCheck, faTimes, faFileInvoiceDollar)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(money, {precision: 2})

Vue.component('paginate', Paginate)

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
