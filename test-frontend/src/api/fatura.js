import axios from 'axios';

export default {
    getFaturas (page) {
        let url;
        if (page) {
            url = `http://localhost:3000/fatura?page=${page}`
        } else {
            url = `http://localhost:3000/fatura`
        }
        return axios({
            method: 'get',
            url,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    getFatura (faturaId) {
        return axios({
            method: 'get',
            url: `http://localhost:3000/fatura/${faturaId}`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    createFatura (fatura) {
        return axios({
            method: 'post',
            url: 'http://localhost:3000/fatura',
            data: fatura,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    updateFatura (faturaId, fatura) {
        return axios({
            method: 'put',
            url: `http://localhost:3000/fatura/${faturaId}`,
            data: fatura,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    deleteFatura (faturaId) {
        return axios({
            method: 'delete',
            url: `http://localhost:3000/fatura/${faturaId}`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}