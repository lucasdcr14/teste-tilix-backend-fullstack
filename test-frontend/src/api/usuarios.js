import axios from 'axios';

export default {
    getUsuarios (page) {
        let url;
        if (page) {
            url = `http://localhost:3000/usuario?page=${page}`
        } else {
            url = `http://localhost:3000/usuario`
        }
        return axios({
            method: 'get',
            url,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    getUsuario (usuarioId) {
        return axios({
            method: 'get',
            url: `http://localhost:3000/usuario/${usuarioId}`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    addUsuario (usuario) {
        return axios({
            method: 'post',
            url: 'http://localhost:3000/usuario',
            data: usuario,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    updateUsuario (usuarioId, usuario) {
        return axios({
            method: 'put',
            url: `http://localhost:3000/usuario/${usuarioId}`,
            data: usuario,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    },
    deleteUsuario (usuarioId) {
        return axios({
            method: 'delete',
            url: `http://localhost:3000/usuario/${usuarioId}`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}