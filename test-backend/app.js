const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const userRoutes = require('./routes/usuario');
const faturaRoutes = require('./routes/fatura');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use('/usuario', userRoutes);
app.use('/fatura', faturaRoutes);

mongoose.connect('mongodb+srv://lucasdcr14:fqk0QdMvlGGnRPbY@cluster0-mu0ng.mongodb.net/tilix?retryWrites=true')
    .then(result => {
        app.listen(3000, () => {
            console.log('Listening on port 3000');
        });
    })
    .catch(err => {
        console.log(err);
    })
