/**
 * Schema para a tabela Fatura
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const faturaSchema = new Schema(
    {
        nomeEmpresa: {
            type: String,
            required: true
        },
        valor: {
            type: Number,
            required: true
        },
        dataVencimento: {
            type: Date,
            required: true
        },
        paga: {
            type: Boolean,
            required: true
        },
        usuario: {
            type: Schema.Types.ObjectId,
            ref: 'Usuario',
            required: true
        }
    }
);

module.exports = mongoose.model('Fatura', faturaSchema);