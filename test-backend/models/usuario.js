/**
 * Schema para a tabela Usuário
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    senha: {
        type: String,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    faturas: [{
        type: Schema.Types.ObjectId,
        ref: 'Fatura'
    }]
});

module.exports = mongoose.model('Usuario', usuarioSchema);