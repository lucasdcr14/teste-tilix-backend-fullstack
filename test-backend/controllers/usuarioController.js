/**
 * Controller de usuários
 */

const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuario');
const Fatura = require('../models/fatura');

/**
 * Criar usuário
 */
exports.criarUsuario = (req, res, next) => {
    const email = req.body.email;
    const nome = req.body.nome;
    const senha = req.body.senha;
    bcrypt.hash(senha, 12)
        .then(hashedPw => {
            const usuario = new Usuario({
                email: email,
                senha: hashedPw,
                nome: nome
            });
            return usuario.save();
        })
        .then(result => {
            res.status(201).json({ message: 'Usuário criado!' });
        })
        .catch(err => {
            res.status(500).json({ message: 'Problemas ocorreram! Já estamos trabalhando na solução.' })
        });
};

/**
 * Obter usuários
 */
exports.getUsers = (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = 6;
    let totalItems;
    Usuario.find().countDocuments()
        .then(count => {
            totalItems = count;
            return Usuario.find().skip((currentPage - 1) * perPage).limit(perPage)
        })
        .then(users => {
            res.status(201).json({ message: 'Fetched users succesfully', users: users, totalItems: totalItems });
        })
        .catch(err => {
            res.status(500).json({ message: 'Problemas ocorreram! Já estamos trabalhando na solução.' })
        })
};

/**
 * Obter um usuário por um ID
 */
exports.getUser = (req, res, next) => {
    const userId = req.params.usuarioId;
    Usuario.findById(userId)
        .then(usuario => {
            if (!usuario) {
                const err = new Error('Usuário não encontrado.');
                err.statusCode = 404;
                throw err;
            }
            res.status(200).json({ message: 'Usuário encontrado com sucesso!', user: usuario })
        })
        .catch(err => {
            const errorStatusCode = err.statusCode || 500;
            const errorMessage = err.message || 'Problemas ocorreram! Já estamos trabalhando na solução.';
            res.status(errorStatusCode).json({ message: errorMessage })
        })
}

/**
 * Deletar um usuário pelo ID
 */
exports.deleteUser = (req, res, next) => {
    const userId = req.params.usuarioId;
    Usuario.findById(userId).populate('faturas', 'faturas').exec()
        .then(user => {
            if (!user) {
                const err = new Error('Usuário não encontrado.');
                err.statusCode = 404;
                throw err;
            }
            if (user.faturas.length > 0) {                
                const err = new Error('Exclusão proibida. Usuário com boletos.');
                err.statusCode = 500;
                throw err;
            }
            return Usuario.findByIdAndRemove(userId);
        })
        .then(result => {
            res.status(200).json({ message: 'Usuário excluido com sucesso!' })
        })
        .catch(err => {
            const errorStatusCode = err.statusCode || 500;
            const errorMessage = err.message || 'Problemas ocorreram! Já estamos trabalhando na solução.';
            res.status(errorStatusCode).json({ message: errorMessage })
        })
};

/**
 * Atualizar um usuário
 */
exports.updateUser = (req, res, next) => {
    const usuarioId = req.params.usuarioId;
    const nome = req.body.nome;
    const email = req.body.email;
    const senha = req.body.senha;

    Usuario.findById(usuarioId)
        .then(usuario => {
            if (!usuario) {
                res.status(500).json({ message: 'Usuário não encontrado.' })
            }
            bcrypt.hash(senha, 12)
                .then(hashedPw => {
                    usuario.nome = nome;
                    usuario.email = email;
                    usuario.senha = hashedPw;
                    return usuario.save();
                })
                .then(result => {
                    res.status(201).json({ message: 'Usuário atualizado com sucesso!' });
                })
        })
        .catch(err => {
            res.status(500).json({ message: 'Problemas ocorreram! Já estamos trabalhando na solução.' })
        });
}