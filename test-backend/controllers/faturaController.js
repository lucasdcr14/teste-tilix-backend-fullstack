/**
 * Controller de faturas
 */

const Usuario = require('../models/usuario');
const Fatura = require('../models/fatura');

/**
 * Criar uma nova fatura
 */
exports.criarFatura = (req, res, next) => {
    const nomeEmpresa = req.body.nomeEmpresa;
    const valor = req.body.valor;
    const dataVencimento = req.body.dataVencimento;
    const paga = req.body.paga;
    const usuario = req.body.usuario;
    const fatura = new Fatura({
        nomeEmpresa,
        valor,
        dataVencimento,
        paga,
        usuario
    });
    fatura.save()
        .then(result => {
            return Usuario.findById(usuario);
        })
        .then(usuario => {
            usuario.faturas.push(fatura)
            return usuario.save();
        })
        .then(result => {
            res.status(201).json({
                message: 'Fatura criada com sucesso!',
                fatura: fatura
            })
        })
        .catch(err => {
            res.status(500).json({ message: 'Problemas ocorreram! Já estamos trabalhando na solução.' })
        })
},

/**
 * Obter todas as faturas
 */
exports.getFaturas = (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = 6;
    let totalItems;
    Fatura.find().countDocuments()
        .then(count => {
            totalItems = count;
            return Fatura.find().skip((currentPage - 1) * perPage).limit(perPage).populate('usuario', 'nome').exec()
        })
        .then(faturas => {
            res.status(200).json({ message: 'Fetched faturas successfully.', faturas: faturas, totalItems: totalItems })
        })
        .catch(err => {
            res.status(500).json({ message: 'Problemas ocorreram! Já estamos trabalhando na solução.' })
        }) 
};

/**
 * Obter uma fatura pelo ID
 */
exports.getFatura = (req, res, next) => {
    const faturaId = req.params.faturaId;
    Fatura.findById(faturaId)
        .then(fatura => {
            if(!fatura) {
                const err = new Error('Usuário não encontrado.');
                err.statusCode = 404;
                throw err;
            }            
            res.status(200).json({ message: 'Fatura encontrada com sucesso!', fatura: fatura })
        })
        .catch(err => {
            const errorStatusCode = err.statusCode || 500;
            const errorMessage = err.message || 'Problemas ocorreram! Já estamos trabalhando na solução.';
            res.status(errorStatusCode).json({ message: errorMessage })
        }) 
}

/**
 * Deletar uma fatura pelo ID
 */
exports.deleteFatura = (req, res, next) => {
    const faturaId = req.params.faturaId;
    let usuario;
    Fatura.findById(faturaId)
        .then(fatura => {
            if (!fatura) {
                const error = new Error('Fatura não encontrada.');
                error.statusCode = 404;
                throw error;
            }        
            usuario = fatura.usuario; 
            return Fatura.findByIdAndRemove(faturaId);
        })
        .then(result => {
            return Usuario.findById(usuario);
        })
        .then(usuario => {
            usuario.faturas.pull(faturaId);
            return usuario.save();
        })
        .then(result => {
            res.status(200).json({ message: 'Fatura excluida com sucesso!'})
        })
        .catch(err => {
            const errorStatusCode = err.statusCode || 500;
            const errorMessage = err.message || 'Problemas ocorreram! Já estamos trabalhando na solução.';
            res.status(errorStatusCode).json({ message: errorMessage })
        })
};

/**
 * Atualizar uma fatura pelo ID
 */
exports.updateFatura = (req, res, next) => {
    const faturaId = req.params.faturaId;
    const nomeEmpresa = req.body.nomeEmpresa;
    const valor = req.body.valor;
    const dataVencimento = req.body.dataVencimento;
    const paga = req.body.paga;
    const usuario = req.body.usuario; 

    let usuarioAntigo;
    
    Fatura.findById(faturaId)
        .then(fatura => {
            if (!fatura) {
                const error = new Error('Fatura não encontrada.');
                error.statusCode = 404;
                throw error;
            }        

            usuarioAntigo = fatura.usuario;

            fatura.nomeEmpresa = nomeEmpresa;
            fatura.valor = valor;
            fatura.dataVencimento = dataVencimento;
            fatura.paga = paga;
            fatura.usuario = usuario;
            return fatura.save();
        })
        .then(result => {
            return Usuario.findById(usuarioAntigo);
        })
        .then(usuarioAntigo => {
            usuarioAntigo.faturas.pull(faturaId);
            return usuarioAntigo.save();            
        })
        .then (result => {
            return Usuario.findById(usuario);
        })
        .then(usuario => {
            usuario.faturas.push(faturaId)
            return usuario.save();
        })
        .then(faturas => {
            res.status(200).json({ message: 'Atualização feita com sucesso'})
        })
        .catch(err => {
            const errorStatusCode = err.statusCode || 500;
            const errorMessage = err.message || 'Problemas ocorreram! Já estamos trabalhando na solução.';
            res.status(errorStatusCode).json({ message: errorMessage })
        })
}

