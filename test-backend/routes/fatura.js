/**
 * Rotas para o recurso Fatura
 */

const express = require('express');

const Fatura = require('../models/fatura');
const FaturaController = require('../controllers/faturaController');

const router = express.Router();

router.post('/', FaturaController.criarFatura);

router.get('/', FaturaController.getFaturas);

router.get('/:faturaId', FaturaController.getFatura);

router.delete('/:faturaId', FaturaController.deleteFatura);

router.put('/:faturaId', FaturaController.updateFatura);

module.exports = router;