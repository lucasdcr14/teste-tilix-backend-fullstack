/**
 * Rotas para o recurso Usuário
 */

const express = require('express');

const UsuarioController = require('../controllers/usuarioController');

const router = express.Router();

router.post('/', UsuarioController.criarUsuario);

router.get('/', UsuarioController.getUsers);

router.get('/:usuarioId', UsuarioController.getUser);

router.delete('/:usuarioId', UsuarioController.deleteUser);

router.put('/:usuarioId', UsuarioController.updateUser);

module.exports = router;